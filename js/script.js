// Завдання
// Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:

// Отримати за допомогою модального вікна браузера два числа.
// Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
// Створити функцію, в яку передати два значення та операцію.
// Вивести у консоль результат виконання функції.

// Необов'язкове завдання підвищеної складності

// Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів числа, або при вводі вказав не числа, - запитати обидва числа знову (при цьому значенням за замовчуванням для кожної зі змінних має бути введена інформація раніше).
let userFirstNumber;
let userSecondNumber;
let mathSymbol;

do {
  userFirstNumber = +prompt("Enter first number");
} while (isNaN(userFirstNumber));

do {
  userSecondNumber = +prompt("Enter second number");
} while (isNaN(userSecondNumber));

do {
  mathSymbol = prompt("Enter Math Symbol (+, -, *, /):");
} while (!mathSymbol || !mathSymbol.match(/[\-, +, *, /]/g));

function countUserNum(userFirstNumber, userSecondNumber, operator) {
  switch (operator) {
    case "+":
      return userFirstNumber + userSecondNumber;
    case "-":
      return userFirstNumber - userSecondNumber;
    case "*":
      return userFirstNumber * userSecondNumber;
    case "/":
      return userFirstNumber / userSecondNumber;
  }
}

let result = countUserNum(userFirstNumber, userSecondNumber, mathSymbol);
console.log(result);

// Другий варіант 

// let x = prompt("Enter the number");
// let y = prompt("Enter next number");
// let z = prompt("Enter math symbol such as +,-,*,/:");
// let result;

// switch (z) {
// case "+": result = x + y;
// break;
// case "-": result = x-y;
// break;
// case "*": result = x*y;
// break;
// case "/": result = x/y;
// break;
// default: alert("Invalid math symbol");
// }
// alert(result);

